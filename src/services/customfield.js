/* jshint esversion: 6 */

import Vue from 'vue';

export const getCustomFields = (payload) => {
    return Vue.http.get('/customfields', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addCustomField = (payload) => {
    return Vue.http.post('/customfields', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewCustomField = (payload) => {
    return Vue.http.get('/customfields/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editCustomField = (payload) => {
    return Vue.http.put('/customfields/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteCustomField = (payload) => {
    return Vue.http.delete('/customfields/'+payload.id, {data: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};