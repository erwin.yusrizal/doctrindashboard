/* jshint esversion: 6 */

import Vue from 'vue';

export const getMe = (payload) => {
    return Vue.http.get('/users/me', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editMe = (payload) => {
    return Vue.http.put('/users/me', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getUsers = (payload) => {
    return Vue.http.get('/users', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addUser = (payload) => {
    return Vue.http.post('/users', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewUser = (payload) => {
    return Vue.http.get('/users/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editUser = (payload) => {
    return Vue.http.put('/users/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteUser = (payload) => {
    return Vue.http.delete('/users/'+payload.id, {data: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};