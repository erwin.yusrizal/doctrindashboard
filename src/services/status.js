/* jshint esversion: 6 */

import Vue from 'vue';

export const getStatuses = (payload) => {
    return Vue.http.get('/statuses', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addStatus = (payload) => {
    return Vue.http.post('/statuses', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewStatus = (payload) => {
    return Vue.http.get('/statuses/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editStatus = (payload) => {
    return Vue.http.put('/statuses/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteStatus = (payload) => {
    return Vue.http.delete('/statuses/'+payload.id, {data: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};