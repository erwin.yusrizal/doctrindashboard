/* jshint esversion:6 */

import Vue from 'vue';
import store from '../store';

export const login = (payload) => {
    return Vue.http.post('/auth/login', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const verify = (payload) => {
    return Vue.http.post("/auth/verify", payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const reverify = (payload) => {
    return Vue.http.put("/auth/verify", payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const refresh = () => {
    let { refresh_token } = store.getters['auth/token'];
    return Vue.http.put("/auth/refresh", {
            headers: {
                'Authorization': 'Bearer ' + refresh_token
            }
        })
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const accessRevoke = () => {
    let { access_token } = store.getters['auth/token'];
    return Vue.http.delete("/auth/revoke/access", {
        headers: {
            'Authorization': 'Bearer ' + access_token
        }
    })
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const refreshRevoke = () => {
    let { refresh_token } = store.getters['auth/token'];
    return Vue.http.delete("/auth/revoke/refresh", {
        headers: {
            'Authorization': 'Bearer ' + refresh_token
        }
    })
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};