/* jshint esversion: 6 */

import Vue from 'vue';

export const getMedias = (payload) => {
    return Vue.http.get('/media/tmp', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addMedia = (payload) => {
    return Vue.http.post('/media/tmp', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewMedia = (payload) => {
    return Vue.http.get('/media/tmp/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteMedia = (payload) => {
    return Vue.http.delete('/media/tmp/'+payload.id, {data: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addMediaDocument = (payload) => {
    return Vue.http.post('/media/documents/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteMediaDocument = (payload) => {
    return Vue.http.delete('/media/documents/'+payload.id, {data: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};