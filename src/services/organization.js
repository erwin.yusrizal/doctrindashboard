/* jshint esversion: 6 */

import Vue from 'vue';

export const getOrganizations = (payload) => {
    return Vue.http.get('/organizations', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addOrganization = (payload) => {
    return Vue.http.post('/organizations', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewOrganization = (payload) => {
    return Vue.http.get('/organizations/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editOrganization = (payload) => {
    return Vue.http.put('/organizations/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteOrganization = (payload) => {
    return Vue.http.delete('/organizations/'+payload.id, {data: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};