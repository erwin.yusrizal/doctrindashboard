/* jshint esversion: 6 */

import Vue from 'vue';

export const getPermissions = (payload) => {
    return Vue.http.get('/permissions', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addPermission = (payload) => {
    return Vue.http.post('/permissions', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewPermission = (payload) => {
    return Vue.http.get('/permissions/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editPermission = (payload) => {
    return Vue.http.put('/permissions/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deletePermission = (payload) => {
    return Vue.http.delete('/permissions/'+payload.id, {data: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};