/* jshint esversion: 6 */

import Vue from 'vue';

export const getHistories = (payload) => {
    return Vue.http.get('/histories', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};