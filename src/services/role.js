/* jshint esversion: 6 */

import Vue from 'vue';

export const getPermissions = (payload) => {
    return Vue.http.get('/permissions', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getRoles = (payload) => {
    return Vue.http.get('/roles', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const addRole = (payload) => {
    return Vue.http.post('/roles', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewRole = (payload) => {
    return Vue.http.get('/roles/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editRole = (payload) => {
    return Vue.http.put('/roles/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editRolePermission = (payload) => {
    return Vue.http.put('/roles/'+payload.id+'/permissions', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteRole = (payload) => {
    return Vue.http.delete('/roles/'+payload.id, {data: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};