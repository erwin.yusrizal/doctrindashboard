/* jshint esversion: 6 */

import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/* Layout */
import Layout from '@/views/layout/Layout';


/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
	roles: ['admin','editor']    will control the page roles (you can set multiple roles)
	title: 'title'               the name show in sub-menu and breadcrumb (recommend set)
	icon: 'icon class' 	         the icon show in the sidebar
	noCache: true                if true, the page will no be cached(default is false)
	breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
	affix: true                  if true, the tag will affix in the tags-view
  }
**/

export const baseRoutes = [{
	path: '/redirect',
	component: Layout,
	hidden: true,
	children: [{
		path: '/redirect/:path*',
		component: () => import('@/views/redirect/index')
	}]
},{
	path: '/login',
	component: () => import('@/views/login/index'),
	hidden: true
},{
	path: '/auth-redirect',
	component: () => import('@/views/login/authredirect'),
	hidden: true
},{
	path: '/404',
	component: () => import('@/views/error/404'),
	hidden: true
},{
	path: '/401',
	component: () => import('@/views/error/401'),
	hidden: true
},{
	path: '',
	component: Layout,
	redirect: 'dashboard',
	children: [{
		path: 'dashboard',
		component: () => import('@/views/dashboard/index'),
		name: 'Dashboard',
		meta: { 
			title: 'Dashboard', 
			icon:'ion-ios-apps',
			color: '#1989fa',
			noCache: true, 
			affix: true 
		}
	}]
}];

export const asyncRoutes = [{
	path: '/me',
	component: Layout,
	redirect: '/me/account',
	hidden: true,
	meta: {
		title: 'Account',
		icon: 'ion-ios-person'
	},
	children: [{
		path: 'account',
        component: () => import('@/views/me/account'),
        name: 'myAccount',
        meta: {
			title: 'My Account'
        }
	}, {
		path: 'histories',
        component: () => import('@/views/me/histories'),
        name: 'myHistories',
        meta: {
			title: 'My Histories'
        }
	}]

}, {
	path: '/documents',
	component: Layout,
	redirect: '/documents/active',
	alwaysShow: true,
	meta: {
		title: 'Document',
		icon: 'ion-ios-document',
		color: '#C8B2F4',
		roles: ['root', 'employee']
	},
	children: [{
		path: 'onprogress',
        component: () => import('@/views/document/documents'),
        name: 'onprogress',
        meta: {
			title: 'On Progress',
			roles: ['root', 'employee'],
			module: 'document'
		}
	}, {
		path: 'archives',
		component: () => import('@/views/document/archives'),
		name: 'archives',
		meta: {
			title: 'Archived',
			roles: ['root', 'employee'],
			module: 'document'
		}
	}, {
		path: 'add',
        component: () => import('@/views/document/documentForm'),
		name: 'add',
		hidden: true,
        meta: {
			title: 'Add New Document',
			roles: ['root', 'employee'],
			module: 'document'
		}
	}, {
		path: ':id',
        component: () => import('@/views/document/documentForm'),
		name: 'edit',
		hidden: true,
        meta: {
			title: 'Edit Document',
			roles: ['root', 'employee'],
			module: 'document'
		}
	}]

}, {
	path: '/people',
	component: Layout,
	redirect: '/people/users',
	alwaysShow: true,
	meta: {
		title: 'People',
		icon: 'ion-ios-person',
		color: '#ED858B',
		roles: ['root']
	},
	children: [{
		path: 'histories',
        component: () => import('@/views/people/histories'),
        name: 'histories',
        meta: {
			title: 'Histories',
			roles: ['root'],
			module: 'history'
		}
	}, {
		path: 'organizations',
        component: () => import('@/views/people/organizations'),
        name: 'organizations',
        meta: {
			title: 'Organizations',
			roles: ['root'],
			module: 'organization'
		}
	}, {
		path: 'permissions',
        component: () => import('@/views/people/permissions'),
        name: 'Permissions',
        meta: {
			title: 'Permissions',
			roles: ['root'],
			module: 'permission'
		}
	}, {
		path: 'roles',
        component: () => import('@/views/people/roles'),
        name: 'roles',
        meta: {
			title: 'Roles',
			roles: ['root'],
			module: 'role'
		}
	}, {
		path: 'users',
        component: () => import('@/views/people/users'),
        name: 'users',
        meta: {
			title: 'Users',
			roles: ['root'],
			module: 'user'
		}
	}, ]
}, {
	path: '/setting',
	component: Layout,
	redirect: '/setting/customfield',
	alwaysShow: true,
	meta: {
		title: 'Setting',
		icon: 'ion-ios-construct',
		color: '#6FC5AA',
		roles: ['root', 'employee']
	},
	children: [{
		path: 'customfield',
        component: () => import('@/views/setting/customfields'),
        name: 'customfield',
        meta: {
			title: 'Custom Fields',
			roles: ['root', 'employee'],
			module: 'customfield'
		}
	}, {
		path: 'status',
        component: () => import('@/views/setting/status'),
        name: 'status',
        meta: {
			title: 'Status',
			roles: ['root', 'employee'],
			module: 'status'
		}
	}]
}, { path: '*', redirect: '/404', hidden: true }];


const createRouter = () => new Router({
	routes: baseRoutes,
	mode: 'history',
	scrollBehavior: () => ({ y: 0 }),
});

const router = createRouter();

export function resetRouter() {
	const newRouter = createRouter();
	router.matcher = newRouter.matcher;
}
  
export default router;