/* jshint esversion:6 */

import Vue from 'vue';
import store from './store';

if (process.env.NODE_ENV === 'production') {
	Vue.config.errorHandler = function(err, vm, info) {
		Vue.nextTick(() => {
			store.dispatch('addErrorLog', {
				err,
				vm,
				info,
				url: window.location.href
			});
		});
	};
}