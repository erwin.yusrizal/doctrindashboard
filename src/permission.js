/* jshint esversion:6 */

import router from './router';
import store from './store';
import { Message } from 'element-ui';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import getPageTitle from '@/utils/page-title';

NProgress.configure({showSpinner: false});

const whiteList = ['/login', '/auth-redirect'];

router.beforeEach((to, from, next) => {
    
    NProgress.start();

    document.title = getPageTitle(to.meta.title);

    if(store.getters['auth/isLoggedIn']){
        if(to.path === '/login'){

            next({ path: '/' });
            NProgress.done();

        }else{

            const me  = store.getters && store.getters['user/me'];

            if(me != null){
                next();
            }else{

                store.dispatch('user/getMe').then((me) => {
                    store.dispatch('permission/generateRoutes', me.role).then((accessRoutes) => {
                        router.addRoutes(accessRoutes);
                        next({...to, replace: true});
                    });
                }).catch((error) => {
                    store.dispatch('auth/doLogout').then(() => {
                        localStorage.removeItem('vuex');
                        Message.error(error);
                        next({ path: '/' });
                        NProgress.done();
                    });
                });
            }
        }
    }else{
        if(whiteList.indexOf(to.path) !== -1){
            next();
        }else{
            next(`/login?redirect=${to.path}`);
            NProgress.done();
        }
    }
});

router.afterEach(() => {
    NProgress.done();
});