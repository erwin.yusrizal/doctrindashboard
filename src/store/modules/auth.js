/* jshint esversion: 6 */

import { login, verify, reverify, accessRevoke, refreshRevoke } from '@/services/auth';
import { parseError } from '@/utils/index';

const auth = {
    namespaced: true,
    state: {
        authStatus: 'login',
        isFailed: false,
        failMessageTitle: '',
        failMessage: '',
        user: null,
        token: null,
        verificationCode: null
    },
    getters: {
        user: state => state.user,
        token: state => state.token,
        authStatus: state => state.authStatus,
        isLoggedIn: state => !!state.token,
        isFailed: state => state.isFailed,
        failMessage: state => state.failMessage,
        failMessageTitle: state => state.failMessageTitle,
        verificationCode: state => state.verificationCode
    },
    mutations: {
        LOGIN_FAILED: (state, payload) => {
            state.authStatus = 'login';
            state.isFailed = true;
            state.failMessageTitle = payload.field;
            state.failMessage = 'Error: ' + payload.messages;
        },
        LOGIN_SUCCESS: (state, payload) => {
            state.authStatus = 'verify';
            state.verificationCode = payload;
            state.isFailed = false;
            state.failMessageTitle = '';
            state.failMessage = '';
        },
        VERIFY_FAILED: (state, payload) => {
            state.isFailed = true;
            state.failMessageTitle = payload.field;
            state.failMessage = 'Error: ' + payload.messages;
        },
        VERIFY_SUCCESS: (state, payload) => {
            state.authStatus = 'loggedin';
            state.user = payload.user;
            state.token = {
                access_token: payload.access_token,
                refresh_token: payload.refresh_token
            };
            state.verificationCode = null;
        },
        REVERIFY_FAILED: (state, payload) => {
            state.isFailed = true;
            state.failMessageTitle = payload.field;
            state.failMessage = 'Error: ' + payload.messages;
        },
        REVERIFY_SUCCESS: (state, payload) => {
            state.authStatus = 'verify';
            state.verificationCode = payload;
        },
        LOGOUT_SUCCESS: (state) => {
            state.authStatus = 'login';
        }
    },
    actions: {
        async doLogin({ commit }, payload){
            try{
                let response = await login(payload);
                let data = response.data;
                commit('LOGIN_SUCCESS', data.verification_code);
                return data;
            }catch(e){
                let error = parseError(e.errors);
                commit('LOGIN_FAILED', error);
            }         
        },
        async doVerify({ commit }, payload){
            try{
                let response = await verify(payload);
                let data = response.data;
                commit('VERIFY_SUCCESS', data);
                return data;
            }catch(e){
                let error = parseError(e.errors);
                commit('VERIFY_FAILED', error);
            }         
        },
        async doReverify({ commit }, payload){
            try{
                let response = await reverify(payload);
                let data = response.data;
                commit('REVERIFY_SUCCESS', data.verification_code);
                return data;
            }catch(e){
                let error = parseError(e.errors);
                commit('REVERIFY_FAILED', error);
            }         
        },
        async doLogout({ dispatch }){
            dispatch('revokeAccess');
            dispatch('revokeRefresh');
        },
        async revokeAccess(){
            try{
                let response = await accessRevoke();
                return response;
            }catch(e){
                // eslint-disable-next-line no-console
                console.log(e);
                return e;
            }
        },
        async revokeRefresh({ commit }){
            commit('LOGOUT_SUCCESS');
            let response = await refreshRevoke();            
            return response;
        }
    }
};

export default auth;