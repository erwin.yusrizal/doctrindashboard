/* jshint esversion: 6 */

import { baseRoutes, asyncRoutes } from '@/router';

import {
    getPermissions,
    addPermission,
    editPermission,
    deletePermission
} from '../../services/permission';

function hasPermission(role, route){
    if(route.meta && route.meta.module){
        return !!role.permissions[route.meta.module].menu;
    }else{
        return true;
    }
}

export function filterAsyncRoutes(routes, role){
    const res = [];
    routes.forEach(route => {
        const tmp = {...route};
        if(hasPermission(role, tmp)){
            if(tmp.children){
                tmp.children = filterAsyncRoutes(tmp.children, role);
            }
            res.push(tmp);
        }
    });

    return res;
}

const permission = {
    namespaced: true,
    state: {
        routes: [],
        addRoutes: [],
        permissions: [],
        pagination: {}
    },
    getters: {
        permissionRoutes: state => state.routes,
        permissions: state => state.permissions,
        pagination : state => state.pagination
    },
    mutations: {
        SET_ROUTES: (state, routes) => {
            state.addRoutes = routes;
            state.routes = baseRoutes.concat(routes);
        },
        ADD_PERMISSIONS: (state, payload) => {
            state.permissions = payload.permissions;
            state.pagination = payload.pagination;
        },
        ADD_MORE_PERMISSIONS: (state, payload) => {
            state.permissions = state.permissions.concat(payload.permissions);
            state.pagination = payload.pagination;
        },
        ADD_PERMISSION: (state, payload) => {
            state.permissions.push(payload);
        },
        VIEW_PERMISSION: (state, payload) => {
            state.permission = payload;
        },
        EDIT_PERMISSION: (state, payload) => {
            for(let i of state.permissions){
                if(i.id == payload.id){
                    i = Object.assign(i, payload);
                    break;
                }
            }
        },
        DELETE_PERMISSION: (state, payload) => {
            for(let i of state.permissions){
                if(i.id == payload.id){
                    const index = state.permissions.indexOf(i);
                    state.permissions.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async generateRoutes({ commit }, role){
            let accessedRoutes;
            if(role.slug == 'root'){
                accessedRoutes = asyncRoutes || [];
            }else{
                accessedRoutes = await filterAsyncRoutes(asyncRoutes, role);
            }
            commit('SET_ROUTES', accessedRoutes);
            return accessedRoutes;
        },
        async getPermissions({ commit }, payload){
            try{
                let response = await getPermissions(payload);
                let data = response.data;

                if(payload.loadmore){
                    commit('ADD_MORE_PERMISSIONS', data);
                }else{
                    commit('ADD_PERMISSIONS', data);
                }
                return data;
            }catch(e){
                return e;
            }
        },
        async addPermission({ commit }, payload){
            try{
                let response = await addPermission(payload);
                let data = response.data;

                commit('ADD_PERMISSION', data.permission);
                return response;
            }catch(e){
                return e;
            }
        },
        async editPermission({ commit }, payload){
            try{
                let response = await editPermission(payload);
                let data = response.data;

                commit('EDIT_PERMISSION', data.permission);
                return response;
            }catch(e){
                return e;
            }
        },
        async deletePermission({ commit }, payload){
            try{
                let response = await deletePermission(payload);
                let data = response.data;

                commit('DELETE_PERMISSION', data.permission);
                return response;
            }catch(e){
                return e;
            }
        }
    }
};

export default permission;