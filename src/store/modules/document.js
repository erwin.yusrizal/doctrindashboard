/* jshint esversion: 6 */

import {
    getDocuments,
    addDocument,
    viewDocument,
    editDocument,
    deleteDocument,
    trackDocument,
    unarchiveDocument
} from '../../services/document';

const document = {
    namespaced: true,
    state: {
        documents: [],
        document: null,
        pagination: {}
    },
    getters: {
        documents: state => state.documents,
        document: state => state.document,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_DOCUMENTS: (state, payload) => {
            state.documents = payload.documents;
            state.pagination = payload.pagination;
        },
        ADD_MORE_DOCUMENTS: (state, payload) => {
            state.documents = state.documents.concat(payload.documents);
            state.pagination = payload.pagination;
        },
        ADD_DOCUMENT: (state, payload) => {
            state.documents.push(payload);
        },
        VIEW_DOCUMENT: (state, payload) => {
            state.document = payload;
        },
        EDIT_DOCUMENT: (state, payload) => {
            for(let i of state.documents){
                if(i.id == payload.id){
                    i = Object.assign(i, payload);
                    break;
                }
            }
        },
        DELETE_DOCUMENT: (state, payload) => {
            for(let i of state.documents){
                if(i.id == payload.id){
                    const index = state.documents.indexOf(i);
                    state.documents.splice(index, 1);
                    break;
                }
            }
        },
        RESET_DOCUMENTS: (state, payload) => {
            state.documents = [];
            state.pagination = {};
        },
        RESET_DOCUMENT: (state, payload) => {
            state.document = null;
        }
    },
    actions: {
        async getDocuments({ commit }, payload){
            try{
                let response = await getDocuments(payload);
                let data = response.data;
                if(payload.loadmore){
                    commit('ADD_MORE_DOCUMENTS', data);
                }else{
                    commit('ADD_DOCUMENTS', data);
                }
                return data;
            }catch(e){
                return e;
            }
        },
        async addDocument({ commit }, payload){
            try{
                let response = await addDocument(payload);
                let data = response.data;
                commit('ADD_DOCUMENT', data.document);
                return response;
            }catch(e){
                return e;
            }
        },
        async viewDocument({ commit }, payload){
            try{
                let response = await viewDocument(payload);
                let data = response.data;
                commit('VIEW_DOCUMENT', data.document);
                return data;
            }catch(e){
                return e;
            }
        },
        async editDocument({ commit }, payload){
            try{
                let response = await editDocument(payload);
                let data = response.data;
                commit('EDIT_DOCUMENT', data.document);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteDocument({ commit }, payload){
            try{
                let response = await deleteDocument(payload);
                let data = response.data;
                commit('DELETE_DOCUMENT', data.document);
                return response;
            }catch(e){
                return e;
            }
        },
        async trackDocument({ commit }, payload){
            try{
                let response = await trackDocument(payload);
                let data = response.data;
                commit('VIEW_DOCUMENT', data.document);
                return data;
            }catch(e){
                return e;
            }
        },
        async unarchiveDocument({ commit }, payload){
            try{
                let response = await unarchiveDocument(payload);
                let data = response.data;
                commit('VIEW_DOCUMENT', data.document);
                return response;
            }catch(e){
                return e;
            }
        },
        resetDocuments({ commit }, payload){
            commit('RESET_DOCUMENTS', payload);
        },
        resetDocument({ commit }, payload){
            commit('RESET_DOCUMENT', payload);
        }
    }
};

export default document;