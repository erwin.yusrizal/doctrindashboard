/* jshint esversion: 6 */

import {
    getStatuses,
    addStatus,
    editStatus,
    deleteStatus
} from '../../services/status.js';

const status = {
    namespaced: true,
    state: {
        statuses: [],
        pagination: {}
    },
    getters: {
        statuses: state => state.statuses,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_STATUSES: (state, payload) => {
            state.statuses = payload.statuses;
            state.pagination = payload.pagination;
        },
        ADD_MORE_STATUSES: (state, payload) => {
            state.statuses = state.statuses.concat(payload.statuses);
            state.pagination = payload.pagination;
        },
        ADD_STATUS: (state, payload) => {
            state.statuses.push(payload);
        },
        EDIT_STATUS: (state, payload) => {
            for(let i of state.statuses){
                if(i.id == payload.id){
                    i = Object.assign(i, payload);
                    break;
                }
            }
        },
        DELETE_STATUS: (state, payload) => {
            for(let i of state.statuses){
                if(i.id == payload.id){
                    const index = state.statuses.indexOf(i);
                    state.statuses.splice(index, 1);
                    break;
                }
            }
        },
        RESET_STATUSES: (state, payload) => {
            state.statuses = [];
        }
    },
    actions: {
        async getStatuses({ commit }, payload){
            try{
                let response = await getStatuses(payload);
                let data = response.data;
                if(payload.loadmore){
                    commit('ADD_MORE_STATUSES', data);
                }else{
                    commit('ADD_STATUSES', data);
                }
                return data;
            }catch(e){
                return e;
            }
        },
        async addStatus({ commit }, payload){
            try{
                let response = await addStatus(payload);
                let data = response.data;
                commit('ADD_STATUS', data.status);
                return response;
            }catch(e){
                return e;
            }
        },
        async editStatus({ commit }, payload){
            try{
                let response = await editStatus(payload);
                let data = response.data;
                commit('EDIT_STATUS', data.status);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteStatus({ commit }, payload){
            try{
                let response = await deleteStatus(payload);
                let data = response.data;
                commit('DELETE_STATUS', data.status);
                return response;
            }catch(e){
                return e;
            }
        },
        resetStatuses({ commit }, payload){
            commit('RESET_STATUSES', payload);
        }
    }
};

export default status;