/* jshint esversion: 6 */

const tagsView = {
    namespaced: true,
    state: {
        visitedViews: [],
        cachedViews: []
    },
    getters: {
        visitedViews: state => state.visitedViews,
        cachedViews: state => state.cachedViews
    },
    mutations: {
        ADD_VISITED_VIEW: (state, view) => {
            if(state.visitedViews.some(v => v.path === view.path)) return;
            state.visitedViews.push(
                Object.assign({}, view, {
                    title: view.meta.title || 'no-name'
                })
            );
        },
        ADD_CACHED_VIEW: (state, view) => {
            if(state.cachedViews.includes(view.name)) return;
            if(!view.meta.noCache){
                state.cachedViews.push(view.name);
            }
        },
        DELETE_VISITED_VIEW: (state, view) => {
            for(const [i, v] of state.visitedViews.entries()){
                if(v.path === view.path){
                    state.visitedViews.splice(i, 1);
                    break;
                }
            }
        },
        DELETE_CACHED_VIEW: (state, view) => {
            for(const i of state.cachedViews){
                if(i === view.name){
                    const index = state.cachedViews.indexOf(i);
                    state.cachedViews.splice(index, 1);
                    break;
                }
            }
        },
        DELETE_OTHERS_VISITED_VIEWS: (state, view) => {
            state.visitedViews = state.visitedViews.filter(v => {
                return v.meta.affix || v.path === view.path;
            });
        },
        DELETE_OTHERS_CACHED_VIEWS: (state, view) => {
            for(const i of state.cachedViews){
                if(i === view.name){
                    const index = state.cachedViews.indexOf(i);
                    state.cachedViews = state.cachedViews.slice(index, index + 1);
                    break;
                }
            }
        },
        DELETE_ALL_VISITED_VIEWS: state => {
            const affixTags = state.visitedViews.filter(tag => tag.meta.affix);
            state.visitedViews = affixTags;
        },
        DELETE_ALL_CACHED_VIEWS: state => {
            state.cachedViews = [];
        },
        UPDATE_VISITED_VIEW: (state, view) => {
            for(let v of state.visitedViews){
                if(v.path === view.path){
                    v = Object.assign(v, view);
                    break;
                }
            }
        }
    },
    actions: {
        addView({ dispatch }, view){
            dispatch('addVisitedView', view);
            dispatch('addCachedView', view);
        },
        addVisitedView({ commit }, view){
            commit('ADD_VISITED_VIEW', view);
        },
        addCachedView({ commit }, view){
            commit('ADD_CACHED_VIEW', view);
        },
        deleteView({ dispatch, state }, view){
            return new Promise(resolve => {
                dispatch('deleteVisitedView', view);
                dispatch('deleteCachedView', view);
                resolve({
                    visitedViews: [...state.visitedViews],
                    cachedViews: [...state.cachedViews]
                });
            });
        },
        deleteVisitedView({ commit, state }, view){
            return new Promise(resolve => {
                commit('DELETE_VISITED_VIEW', view);
                resolve([...state.visitedViews]);
            });
        },
        deleteCachedView({ commit, state }, view){
            return new Promise(resolve => {
                commit('DELETE_CACHED_VIEW', view);
                resolve([...state.cachedViews]);
            });
        },
        deleteOthersViews({ dispatch, state }, view){
            return new Promise(resolve => {
                dispatch('deleteOthersVisitedViews', view);
                dispatch('deleteOthersCachedViews', view);
                resolve({
                    visitedViews: [...state.visitedViews],
                    cachedViews: [...state.cachedViews]
                });
            });
        },
        deleteOthersVisitedViews({ commit, state }, view){
            return new Promise(resolve => {
                commit('DELETE_OTHERS_VISITED_VIEWS', view);
                resolve([...state.visitedViews]);
            });
        },
        deleteOthersCachedViews({ commit, state }, view){
            return new Promise(resolve => {
                commit('DELETE_OTHERS_CACHED_VIEWS', view);
                resolve([...state.cachedViews]);
            });
        },
        deleteAllViews({ dispatch, state }, view){
            return new Promise(resolve => {
                dispatch('deleteAllVisitedViews', view);
                dispatch('deleteAllCachedViews', view);
                resolve({
                    visitedViews: [...state.visitedViews],
                    cachedViews: [...state.cachedViews]
                });
            });
        },
        deleteAllVisitedViews({ commit, state }, view){
            return new Promise(resolve => {
                commit('DELETE_ALL_VISITED_VIEWS', view);
                resolve([...state.visitedViews]);
            });
        },
        deleteAllCachedViews({ commit, state }, view){
            return new Promise(resolve => {
                commit('DELETE_ALL_CACHED_VIEWS', view);
                resolve([...state.cachedViews]);
            });
        },
        updateVisitedView({ commit }, view){
            commit('UPDATE_VISITED_VIEW', view);
        }
    }
};

export default tagsView;