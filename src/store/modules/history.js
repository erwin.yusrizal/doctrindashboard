/* eslint-disable no-console */
/* jshint esversion: 6 */

import {
    getHistories
} from '../../services/history';

const history = {
    namespaced: true,
    state: {
        histories: [],
        pagination: []
    },
    getters: {
        histories: state => state.histories,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_HISTORIES: (state, payload) => {
            state.histories = payload.histories;
            state.pagination = payload.pagination;
        },
        ADD_MORE_HISTORIES: (state, payload) => {
            state.histories = state.histories.concat(payload.histories);
            state.pagination = payload.pagination;
        }
    },
    actions: {
        async getHistories({commit}, payload){
            try{
                let response = await getHistories(payload);
                let data = response.data;
                if(payload.loadmore){
                    commit('ADD_MORE_HISTORIES', data);
                }else{
                    commit('ADD_HISTORIES', data);
                }
                return data;
            }catch(e){
                return e;
            }
        }
    }

};

export default history;