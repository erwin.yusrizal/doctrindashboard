/* jshint esversion: 6 */

import {
    getCustomFields,
    addCustomField,
    editCustomField,
    deleteCustomField
} from '../../services/customfield';

const customfield = {
    namespaced: true,
    state: {
        customFields: [],
        pagination: {}
    },
    getters: {
        customFields: state => state.customFields,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_CUSTOM_FIELDS: (state, payload) => {
            state.customFields = payload.groups;
            state.pagination = payload.pagination;
        },
        ADD_MORE_CUSTOM_FIELDS: (state, payload) => {
            state.customFields = state.customFields.concat(payload.groups);
            state.pagination = payload.pagination;
        },
        ADD_CUSTOM_FIELD: (state, payload) => {
            state.customFields.push(payload);
        },
        EDIT_CUSTOM_FIELD: (state, payload) => {
            for(let i of state.customFields){
                if(i.id == payload.id){
                    i = Object.assign(i, payload);
                    break;
                }
            }
        },
        DELETE_CUSTOM_FIELD: (state, payload) => {
            for(let i of state.customFields){
                if(i.id == payload.id){
                    const index = state.customFields.indexOf(i);
                    state.customFields.splice(index, 1);
                    break;
                }
            }
        },
        RESET_CUSTOM_FIELDS: (state, payload) => {
            state.customFields = [];
        }
    },
    actions: {
        async getCustomFields({ commit }, payload){
            try{
                let response = await getCustomFields(payload);
                let data = response.data;
                if(payload.loadmore){
                    commit('ADD_MORE_CUSTOM_FIELDS', data);
                }else{
                    commit('ADD_CUSTOM_FIELDS', data);
                }
                return data;
            }catch(e){
                return e;
            }
        },
        async addCustomField({ commit }, payload){
            try{
                let response = await addCustomField(payload);
                let data = response.data;
                commit('ADD_CUSTOM_FIELD', data.group);
                return response;
            }catch(e){
                return e;
            }
        },
        async editCustomField({ commit }, payload){
            try{
                let response = await editCustomField(payload);
                let data = response.data;
                commit('EDIT_CUSTOM_FIELD', data.group);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteCustomField({ commit }, payload){
            try{
                let response = await deleteCustomField(payload);
                let data = response.data;
                commit('DELETE_CUSTOM_FIELD', data.group);
                return response;
            }catch(e){
                return e;
            }
        },
        resetCustomFields({ commit }, payload){
            commit('RESET_CUSTOM_FIELDS', payload);
        }
    }
};

export default customfield;