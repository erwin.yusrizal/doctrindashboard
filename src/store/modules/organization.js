/* jshint esversion: 6 */

import {
    getOrganizations,
    addOrganization,
    editOrganization,
    deleteOrganization
} from '../../services/organization';

const organization = {
    namespaced: true,
    state: {
        organizations: [],
        pagination: {},
        filterOrganizations: []
    },
    getters: {
        organizations: state => state.organizations,
        pagination: state => state.pagination,
        filterOrganizations: state => state.filterOrganizations
    },
    mutations: {
        ADD_ORGANIZATIONS: (state, payload) => {
            if(payload.type == 'filter'){
                state.filterOrganizations = payload.organizations;
            }else{
                state.organizations = payload.organizations;
                state.pagination = payload.pagination;
            }
        },
        ADD_MORE_ORGANIZATIONS: (state, payload) => {
            state.organizations = state.organizations.concat(payload.organizations);
            state.pagination = payload.pagination;
        },
        ADD_ORGANIZATION: (state, payload) => {
            state.organizations.push(payload);
        },
        EDIT_ORGANIZATION: (state, payload) => {
            for(let i of state.organizations){
                if(i.id == payload.id){
                    i = Object.assign(i, payload);
                    break;
                }
            }
        },
        DELETE_ORGANIZATION: (state, payload) => {
            for(let i of state.organizations){
                if(i.id == payload.id){
                    const index = state.organizations.indexOf(i);
                    state.organizations.splice(index, 1);
                    break;
                }
            }
        },
        RESET_ORGANIZATIONS: (state, payload) => {
            if(payload.type == 'filter'){
                state.filterOrganizations = [];
            }else{
                state.organizations = [];
                state.pagination = {};
            }
        }
    },
    actions: {
        async getOrganizations({ commit }, payload){
            try{
                let response = await getOrganizations(payload);
                let data = response.data;
                data.type = payload.type;
                if(payload.loadmore){
                    commit('ADD_MORE_ORGANIZATIONS', data);
                }else{
                    commit('ADD_ORGANIZATIONS', data);
                }
                return data;
            }catch(e){
                return e;
            }
        },
        async addOrganization({ commit }, payload){
            try{
                let response = await addOrganization(payload);
                let data = response.data;
                commit('ADD_ORGANIZATIONS', data.organization);
                return response;
            }catch(e){
                return e;
            }
        },
        async editOrganization({ commit }, payload){
            try{
                let response = await editOrganization(payload);
                let data = response.data;
                commit('EDIT_ORGANIZATION', data.organization);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteOrganization({ commit }, payload){
            try{
                let response = await deleteOrganization(payload);
                let data = response.data;
                commit('DELETE_ORGANIZATION', data.organization);
                return response;
            }catch(e){
                return e;
            }
        },
        resetOrganizations({ commit }, payload){
            commit('RESET_ORGANIZATIONS', payload);
        }
    }
};

export default organization;