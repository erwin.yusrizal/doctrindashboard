/* eslint-disable no-console */
/* jshint esversion: 6 */

import { 
    getMe, 
    editMe,
    getUsers, 
    addUser, 
    viewUser,
    editUser, 
    deleteUser 
} from '@/services/user';

const user = {
    namespaced: true,
    state: {
        me: null,
        users: [],
        authors: [],
        user: null,
        pagination: {} 
    },
    getters: {
        me: state => state.me,
        users: state => state.users,
        authors: state => state.authors,
        user: state => state.user,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_ME: (state, payload) => {
            state.me = payload;
        },
        ADD_USERS: (state, payload) => {
            state.users = payload.users;
            state.pagination = payload.pagination;
        },
        ADD_AUTHORS: (state, payload) => {
            state.authors = payload.users;
        },
        ADD_MORE_USERS: (state, payload) => {
            state.users = payload.users.concat(payload.users);
            state.pagination = payload.pagination;
        },
        ADD_USER: (state, payload) => {
            state.users.push(payload);
        },
        VIEW_USER: (state, payload) => {
            state.user = payload;
        },
        EDIT_USER: (state, payload) => {
            for(let i of state.users){
                if(i.id == payload.id){
                    i = Object.assign(i, payload);
                    break;
                }
            }
        },
        DELETE_USER: (state, payload) => {
            for(let i of state.users){
                if(i.id == payload.id){
                    const index = state.users.indexOf(i);
                    state.users.splice(index, 1);
                    break;
                }
            }
        },
        EDIT_ME: (state, data) => {
            data.rootState.auth.user = data.user;
        },
        RESET_USERS: (state, payload) => {
            state.users = [];
            state.pagination = {};
        },
        RESET_AUTHORS: (state, payload) => {
            state.authors = [];
        }
    },
    actions: {
        async getMe({ commit }, payload){
            try{
                let response = await getMe(payload);
                let data = response.data;
                commit('ADD_ME', data.user);
                return data.user;
            }catch(e){
                return e;
            }    
        },
        async editMe({ commit, rootState }, payload){
            try{
                let response = await editMe(payload);
                commit('EDIT_ME', {rootState: rootState, user:payload});
                return response;
            }catch(e){
                return e;
            }    
        },
        async getUsers({ commit }, payload){
            try{
                let response = await getUsers(payload);
                let data = response.data;
                if(payload.loadmore){
                    commit('ADD_MORE_USERS', data);
                }else{
                    commit('ADD_USERS', data);
                }                
                return data;
            }catch(e){
                return e;
            }    
        },
        async getAuthors({ commit }, payload){
            try{
                let response = await getUsers(payload);
                let data = response.data;
                commit('ADD_AUTHORS', data);               
                return data;
            }catch(e){
                return e;
            }    
        },
        async addUser({ commit }, payload){
            try{
                let response = await addUser(payload);
                let data = response.data;
                commit('ADD_USER', data.user);
                return response;
            }catch(e){
                return e;
            }
        },
        async viewUser({ commit }, payload){
            try{
                let response = await viewUser(payload);
                let data = response.data;
                commit('VIEW_USER', data.user);
                return response;
            }catch(e){
                return e;
            }
        },
        async editUser({ commit }, payload){
            try{
                let response = await editUser(payload);
                let data = response.data;
                commit('EDIT_USER', data.user);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteUser({ commit }, payload){
            try{
                let response = await deleteUser(payload);
                let data = response.data;
                commit('DELETE_USER', data.user);
                return response;
            }catch(e){
                return e;
            }
        },
        resetUsers({ commit }, payload){
            commit('RESET_USERS', payload);
        },
        resetAuthors({ commit }, payload){
            commit('RESET_AUTHORS', payload);
        }
    }
};

export default user;