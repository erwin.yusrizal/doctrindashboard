/* eslint-disable no-console */
/* jshint esversion: 6 */

import { 
    getMedias, 
    addMedia, 
    viewMedia, 
    deleteMedia, 
    addMediaDocument,
    deleteMediaDocument 
} from '@/services/media';

const media = {
    namespaced: true,
    state: {
        medias: [],
        pagination: {},
        media: null 
    },
    getters: {
        medias: state => state.medias,
        pagination: state => state.pagination,
        media: state => state.media
    },
    mutations: {
        ADD_MEDIAS: (state, payload) => {
            state.medias = payload.medias;
            state.pagination = payload.pagination;
        },
        ADD_MORE_MEDIAS: (state, payload) => {
            state.medias = payload.medias.concat(payload.medias);
            state.pagination = payload.pagination;
        },
        ADD_MEDIA: (state, payload) => {
            state.medias.push(payload);
        },
        VIEW_MEDIA: (state, payload) => {
            state.media = payload;
        },
        DELETE_MEDIA: (state, media) => {
            for(let i of state.medias){
                if(i.id == media.id){
                    const index = state.medias.indexOf(i);
                    state.medias.splice(index, 1);
                    break;
                }
            }
        },
    },
    actions: {
        async getMedias({ commit }, payload){
            try{
                let response = await getMedias(payload);
                let data = response.data;

                if(payload.loadmore){
                    commit('ADD_MORE_MEDIAS', data);
                }else{
                    commit('ADD_MEDIAS', data);
                }                   
                return data;
            }catch(e){
                return e;
            }    
        },
        async addMedia({ commit }, payload){
            try{
                let response = await addMedia(payload);
                let data = response.data;
                commit('ADD_MEDIA', data.media);
                return response;
            }catch(e){
                return e;
            }
        },
        async viewMedia({ commit }, payload){
            try{
                let response = await viewMedia(payload);
                let data = response.data;
                commit('VIEW_MEDIA', data.media);
                return data;
            }catch(e){
                return e;
            }
        },
        async deleteMedia({ commit }, payload){
            try{
                let response = await deleteMedia(payload);
                let data = response.data;
                commit('DELETE_MEDIA', data.media);
                return response;
            }catch(e){
                return e;
            }
        },
        async addMediaDocument({ commit }, payload){
            try{
                let response = await addMediaDocument(payload);
                let data = response.data;
                commit('ADD_MEDIA', data.media);
                return response;
            }catch(e){
                return e;
            }
        },
        async deleteMediaDocument({ commit }, payload){
            try{
                let response = await deleteMediaDocument(payload);
                let data = response.data;
                commit('DELETE_MEDIA', data.media);
                return response;
            }catch(e){
                return e;
            }
        },
    }
};

export default media;