/* jshint esversion: 6 */

import Vue from 'vue';
import Vuex from 'vuex';
import Cookies from 'js-cookie';
import createPersistedState from 'vuex-persistedstate';

import tagsView from './modules/tagsView';
import logs from './modules/logs';
import auth from './modules/auth';
import permission from './modules/permission';
import media from './modules/media';
import role from './modules/role';
import user from './modules/user';
import history from './modules/history';
import organization from './modules/organization';
import status from './modules/status';
import customfield from './modules/customfield';
import document from './modules/document';

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState({
        paths: ['auth.user', 'auth.token']
    })],
    state: {
        sidebar: {
            opened: Cookies.get('sidebar') ? !!+Cookies.get('sidebar') : true,
            withoutAnimation: false
        },
        device: 'desktop'
    },
    getters: {
        sidebar: state => state.sidebar,
        device: state => state.device
    },
    mutations: {
        TOGGLE_SIDEBAR: state => {
            state.sidebar.opened = !state.sidebar.opened;
            state.sidebar.withoutAnimation = false;
            if(state.sidebar.opened){
                Cookies.set('sidebar', 1);
            }else{
                Cookies.set('sidebar', 0);
            }
        },
        CLOSE_SIDEBAR: (state, withoutAnimation) => {
            Cookies.set('sidebar', 0);
            state.sidebar.opened = false;
            state.sidebar.withoutAnimation = withoutAnimation;
        },
        TOGGLE_DEVICE: (state, device) => {
            state.device = device;
        }
    },
    actions: {
        toggleSidebar({ commit }){
            commit('TOGGLE_SIDEBAR');
        },
        closeSidebar({ commit }){
            commit('CLOSE_SIDEBAR');
        },
        toggleDevice({ commit }){
            commit('TOGGLE_DEVICE');
        }
    },
    modules: {
        tagsView,
        logs,
        auth,
        permission,
        media,
        role,
        user,
        history,
        organization,
        status,
        customfield,
        document
    }
});