/* jshint esversion:6 */

import moment from 'moment';

export function validEmail(email) {
	const re = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

export function validPhone(phone){
	return /\(?(?:\+62|62|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}/g.test(phone);
}

export function capitalize(str){
	return str.replace(/\w\S*/g, function(txt){
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}

export function initial(fullname){
	return Array.prototype.map.call(fullname.split(" "), function(x,y){ return y < 2 ? x.substring(0,1).toUpperCase():'';}).join('');
}

export function truncate(str, len){
	len = len || 100;
	return str.length > len ? str.substring(0, len) + '...' : str;
}

export function parseError(e){

	let error = {};

	for(let key in e.messages){
		let field = key.replace('_', ' ');
		error['field'] = capitalize(field);
		error['messages'] = e.messages[key].join(', ');
	}

	return error;
}

export function dateFormat(date, format){
	moment.locale('id_id');
	format = format || 'DD/MM/YYYY HH:mm';
	return moment(date).format(format);
}

export function tableHeight(){
	return document.documentElement.clientHeight - 295;
}

export function numberFormat(number, n, x, s, c) {
	var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
		num = number.toFixed(Math.max(0, ~~n));

	return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
}

export function convertAmpersand(text){
    return text.replace(/&amp;/g, '&');
}

export function hoursAgo(date){
    moment.locale('id_id');
    let now = moment(new Date());
    let end = moment(date);
    let duration = moment.duration(now.diff(end));
    let days = duration.asDays();

    if(days >= 1){
        return moment(date).format('DD MMM YYYY HH:mm');
    }else{
        return moment(date).format('HH:mm');
    }
}

export function stripHTML(str){
    let regex = /(<([^>]+)>)/ig;
    return str.replace(regex, "");
}

export function bytesToSize(bytes) {
	var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	if (bytes == 0) return '0 Byte';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}