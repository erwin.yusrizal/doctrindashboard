/* eslint-disable no-console */
/* jshint esversion: 6 */

import store from '@/store';

export default {
    inserted(el, binding){
        const { value } = binding;
        const { id, role } = store.getters && store.getters['user/me'];
        if (value && value instanceof Array && value.length > 0) {
            const roles = [role];
            const [module, key, val] = value;

            let hasPermission = false;

            if(role.slug == 'root'){
                hasPermission = true;
            }else{
                if(module){
                    // check if module author
                    if(module.author != null && module.author.id == id){
                        hasPermission = true;
                    }else if(val.indexOf('view') > -1 && role.permissions[key]['viewother'] > 0){
                        hasPermission = true;
                    }else if(val.indexOf('edit') > -1 && role.permissions[key]['editother'] > 0){
                        hasPermission = true;
                    }else if(val.indexOf('delete') > -1 && role.permissions[key]['deleteother'] > 0){
                        hasPermission = true;
                    }else{
                        hasPermission = false;
                    }
                }else{
                    hasPermission = roles.some(role => {
                        return role.permissions[key][val] > 0;
                    });
                }
                               
            }
            
            if (!hasPermission) {
                let parent = el.parentNode;

                el.parentNode && el.parentNode.removeChild(el);
                if(module && parent && !parent.hasChildNodes()){
                    parent.innerHTML = '<i class="ion-ios-lock"></i> read only';
                }

            }
        } else {
            throw new Error(`Directive v-permission needs Array permission!"`);
        }
    }
};
